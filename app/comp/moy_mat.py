##############################################################################
# ScoDoc
# Copyright (c) 1999 - 2024 Emmanuel Viennet.  All rights reserved.
# See LICENSE
##############################################################################

"""Calcul des moyennes de matières
"""

# C'est un recalcul (optionnel) effectué _après_ le calcul standard.

import numpy as np
import pandas as pd
from app.comp import moy_ue
from app.models.formsemestre import FormSemestre

from app.scodoc.codes_cursus import UE_SPORT
from app.scodoc.sco_utils import ModuleType


def compute_mat_moys_classic(
    formsemestre: FormSemestre,
    sem_matrix: np.array,
    ues: list,
    modimpl_inscr_df: pd.DataFrame,
    modimpl_coefs: np.array,
) -> dict:
    """Calcul des moyennes par matières.
    Result: dict, { matiere_id : Series, index etudid }
    """
    modimpls_std = [
        m
        for m in formsemestre.modimpls_sorted
        if (m.module.module_type == ModuleType.STANDARD)
        and (m.module.ue.type != UE_SPORT)
    ]
    matiere_ids = {m.module.matiere.id for m in modimpls_std}
    matiere_moy = {}  # { matiere_id : moy pd.Series, index etudid }
    for matiere_id in matiere_ids:
        modimpl_mask = np.array(
            [m.module.matiere.id == matiere_id for m in formsemestre.modimpls_sorted]
        )
        etud_moy_mat = moy_ue.compute_mat_moys_classic(
            sem_matrix=sem_matrix,
            modimpl_inscr_df=modimpl_inscr_df,
            modimpl_coefs=modimpl_coefs,
            modimpl_mask=modimpl_mask,
        )
        matiere_moy[matiere_id] = etud_moy_mat
    return matiere_moy
