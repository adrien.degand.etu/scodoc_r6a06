##############################################################################
# ScoDoc
# Copyright (c) 1999 - 2024 Emmanuel Viennet.  All rights reserved.
# See LICENSE
##############################################################################

"""Cache pour résultats (super classe)
"""

from app.models import FormSemestre


class ResultatsCache:
    """Résultats cachés (via redis)
    L'attribut _cached_attrs donne la liste des noms des attributs à cacher
    (doivent être sérialisables facilement, se limiter à des types simples)

    store() enregistre les attributs dans le cache, et
    load_cached() les recharge.
    """

    _cached_attrs = ()  # virtual

    def __init__(self, formsemestre: FormSemestre, cache_class=None):
        self.formsemestre: FormSemestre = formsemestre
        self.cache_class = cache_class

    def load_cached(self) -> bool:
        "Load cached dataframes, returns False si pas en cache"
        data = self.cache_class.get(self.formsemestre.id)
        if not data:
            return False
        for attr in self._cached_attrs:
            setattr(self, attr, data[attr])
        return True

    def store(self):
        "Cache our data"
        self.cache_class.set(
            self.formsemestre.id,
            {attr: getattr(self, attr) for attr in self._cached_attrs},
        )
