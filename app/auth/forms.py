# -*- coding: UTF-8 -*

"""Formulaires authentification
"""
from urllib.parse import urlparse, urljoin
from flask import request, url_for, redirect
from flask_wtf import FlaskForm
from wtforms import BooleanField, HiddenField, PasswordField, StringField, SubmitField
from wtforms.fields.simple import FileField
from wtforms.validators import ValidationError, DataRequired, Email, EqualTo
from app.auth.models import User, is_valid_password


_ = lambda x: x  # sans babel
_l = _

# See http://flask.pocoo.org/snippets/63/
def is_safe_url(target):
    ref_url = urlparse(request.host_url)
    test_url = urlparse(urljoin(request.host_url, target))
    return test_url.scheme in ("http", "https") and ref_url.netloc == test_url.netloc


def get_redirect_target():
    for target in request.args.get("next"), request.referrer:
        if not target:
            continue
        if is_safe_url(target):
            return target


class RedirectForm(FlaskForm):
    next = HiddenField()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if not self.next.data:
            self.next.data = get_redirect_target() or ""

    def redirect(self, endpoint="index", **values):
        if is_safe_url(self.next.data):
            return redirect(self.next.data)
        target = get_redirect_target()
        return redirect(target or url_for(endpoint, **values))


class LoginForm(RedirectForm):
    user_name = StringField(_l("Nom d'utilisateur"), validators=[DataRequired()])
    password = PasswordField(_l("Mot de passe"), validators=[DataRequired()])
    remember_me = BooleanField(_l("mémoriser la connexion"))
    submit = SubmitField(_l("Suivant"))

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)


class UserCreationForm(FlaskForm):
    user_name = StringField(_l("Nom d'utilisateur"), validators=[DataRequired()])
    email = StringField(_l("Email"), validators=[DataRequired(), Email()])
    password = PasswordField(_l("Mot de passe"), validators=[DataRequired()])
    password2 = PasswordField(
        _l("Répéter"), validators=[DataRequired(), EqualTo("password")]
    )
    submit = SubmitField(_l("Inscrire"))

    def validate_user_name(self, user_name):
        user = User.query.filter_by(user_name=user_name.data).first()
        if user is not None:
            raise ValidationError(_("Please use a different user_name."))

    def validate_email(self, email):
        user = User.query.filter_by(email=email.data).first()
        if user is not None:
            raise ValidationError(_("Please use a different email address."))


class ResetPasswordRequestForm(FlaskForm):
    email = StringField(
        _l("Adresse email associée à votre compte ScoDoc:"),
        validators=[DataRequired(), Email()],
    )
    submit = SubmitField(_l("Envoyer"))


class ResetPasswordForm(FlaskForm):
    password = PasswordField(_l("Mot de passe"), validators=[DataRequired()])
    password2 = PasswordField(
        _l("Répéter"), validators=[DataRequired(), EqualTo("password")]
    )
    submit = SubmitField(_l("Valider ce mot de passe"))

    def validate_password(self, password):
        if not is_valid_password(password.data):
            raise ValidationError(f"Mot de passe trop simple, recommencez")


class DeactivateUserForm(FlaskForm):
    submit = SubmitField("Modifier l'utilisateur")
    cancel = SubmitField(label="Annuler", render_kw={"formnovalidate": True})


class CASUsersImportConfigForm(FlaskForm):
    user_config_file = FileField(
        label="Fichier Excel à réimporter",
        description="""fichier avec les paramètres CAS renseignés""",
    )
    submit = SubmitField("Importer le fichier utilisateurs")
    cancel = SubmitField(label="Annuler", render_kw={"formnovalidate": True})
