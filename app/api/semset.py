##############################################################################
# ScoDoc
# Copyright (c) 1999 - 2024 Emmanuel Viennet.  All rights reserved.
# See LICENSE
##############################################################################

"""
  ScoDoc 9 API : accès aux formsemestres
"""
# from flask import g, jsonify, request
# from flask_login import login_required

# import app
# from app.api import api_bp as bp, api_web_bp, API_CLIENT_ERROR
# from app.decorators import scodoc, permission_required
# from app.scodoc.sco_utils import json_error
# from app.models.formsemestre import NotesSemSet
# from app.scodoc.sco_permissions import Permission


# Impossible de changer la période à cause des archives
# @bp.route("/semset/set_periode/<int:semset_id>", methods=["POST"])
# @api_web_bp.route("/semset/set_periode/<int:semset_id>", methods=["POST"])
# @login_required
# @scodoc
# @permission_required(Permission.EditApogee)
# # TODO à modifier pour utiliser @as_json
# def semset_set_periode(semset_id: int):
#     "Change la période d'un semset"
#     query = NotesSemSet.query.filter_by(semset_id=semset_id)
#     if g.scodoc_dept:
#         query = query.filter_by(dept_id=g.scodoc_dept_id)
#     semset: NotesSemSet = query.first_or_404()
#     data = request.get_json(force=True)  # may raise 400 Bad Request
#     try:
#         periode = int(data)
#         semset.set_periode(periode)
#     except ValueError:
#         return json_error(API_CLIENT_ERROR, "invalid periode value")
#     return jsonify({"OK": True})
