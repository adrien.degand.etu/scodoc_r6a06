# -*- mode: python -*-
# -*- coding: utf-8 -*-

"""Definition of ScoDoc default roles
"""

from app.scodoc.sco_permissions import Permission as p

SCO_ROLES_DEFAULTS = {
    "Ens": (
        p.AbsAddBillet,
        p.AbsChange,
        p.EnsView,
        p.EtudAddAnnotations,
        p.Observateur,
        p.ScoView,
        p.ViewEtudData,
        p.UsersView,
    ),
    "Secr": (
        p.AbsAddBillet,
        p.AbsChange,
        p.EditApogee,
        p.EtudAddAnnotations,
        p.EtudChangeAdr,
        p.Observateur,
        p.ScoView,
        p.UsersView,
        p.ViewEtudData,
    ),
    # Admin est le chef du département, pas le "super admin"
    # on doit donc lister toutes ses permissions:
    "Admin": (
        p.AbsAddBillet,
        p.AbsChange,
        p.EditFormation,
        p.EditPreferences,
        p.EditAllEvals,
        p.EditAllNotes,
        p.EditApogee,
        p.EditFormationTags,
        p.EnsView,
        p.EtudAddAnnotations,
        p.EtudChangeAdr,
        p.EtudChangeGroups,
        p.EtudInscrit,
        p.EditFormSemestre,
        p.Observateur,
        p.ScoView,
        p.UsersAdmin,
        p.UsersView,
        p.ViewEtudData,
    ),
    # Rôles pour l'application relations entreprises
    # ObservateurEntreprise est un observateur de l'application entreprise
    "ObservateurEntreprise": (p.RelationsEntrepView,),
    # UtilisateurEntreprise est un utilisateur de l'application entreprise (droit de modification)
    "UtilisateurEntreprise": (
        p.RelationsEntrepView,
        p.RelationsEntrepEdit,
        p.RelationsEntrepViewCorrs,
    ),
    # AdminEntreprise est un admin de l'application entreprise
    # (toutes les actions possibles de l'application)
    "AdminEntreprise": (
        p.RelationsEntrepView,
        p.RelationsEntrepEdit,
        p.RelationsEntrepExport,
        p.RelationsEntrepSend,
        p.RelationsEntrepValidate,
        p.RelationsEntrepViewCorrs,
    ),
    # LecteurAPI peut utiliser l'API en lecture
    "LecteurAPI": (p.ScoView,),
    "Observateur": (p.Observateur,),
    # RespPE est le responsable poursuites d'études
    # il peut ajouter des tags sur les formations:
    # (doit avoir un rôle Ens en plus !)
    "RespPe": (p.EditFormationTags,),
    # Super Admin est un root: création/suppression de départements
    # _tous_ les droits
    # Afin d'avoir tous les droits, il ne doit pas être asscoié à un département
    "SuperAdmin": p.ALL_PERMISSIONS,
}

# Les rôles accessibles via la page d'admin utilisateurs
# - associés à un département:
ROLES_ATTRIBUABLES_DEPT = ("Ens", "Secr", "Admin", "RespPe")
# - globaux: (ne peuvent être attribués que par un SuperAdmin)
ROLES_ATTRIBUABLES_SCODOC = (
    "ObservateurEntreprise",
    "UtilisateurEntreprise",
    "AdminEntreprise",
    "LecteurAPI",
)
