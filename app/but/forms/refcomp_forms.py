##############################################################################
# ScoDoc
# Copyright (c) 1999 - 2024 Emmanuel Viennet.  All rights reserved.
# See LICENSE
##############################################################################

"""ScoDoc 9 : Formulaires / référentiel de compétence
"""

from flask_wtf import FlaskForm
from flask_wtf.file import FileField, FileAllowed
from wtforms import SelectField, SubmitField


class FormationRefCompForm(FlaskForm):
    referentiel_competence = SelectField(
        "Choisir parmi les référentiels déjà chargés :"
    )
    submit = SubmitField("Valider")
    cancel = SubmitField("Annuler")


class RefCompLoadForm(FlaskForm):
    referentiel_standard = SelectField(
        "Choisir un référentiel de compétences officiel BUT"
    )
    upload = FileField(
        label="... ou bien sélectionner un fichier XML au format Orébut (réservé aux développeurs !)",
        validators=[
            FileAllowed(
                [
                    "xml",
                ],
                "Fichier XML Orébut seulement",
            ),
        ],
    )
    submit = SubmitField("Valider")
    cancel = SubmitField("Annuler")

    def validate(self, extra_validators=None):
        if not super().validate(extra_validators):
            return False
        if (self.referentiel_standard.data == "0") == (not self.upload.data):
            self.referentiel_standard.errors.append(
                "Choisir soit un référentiel, soit un fichier xml"
            )
            return False
        return True
