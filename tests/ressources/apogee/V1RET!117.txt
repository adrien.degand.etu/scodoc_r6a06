XX-APO_TITRES-XX
apoC_annee	2021/2022
apoC_cod_dip	VBTRET
apoC_Cod_Exp	2
apoC_cod_vdi	17
apoC_Fichier_Exp	C:\TEMP\VBTRET-V1RET-V1RETW2.TXT
apoC_lib_dip	BUT R et T
apoC_Titre1	Export Apogée du 17/04/2023 à 13:23
apoC_Titre2	
							
XX-APO_TYP_RES-XX
10	AB1	AB2	ABI	ABJ	ADM	AJ	AJRO	C1	DEF	DIF	
18	AB1	AB2	ABI	ABJ	ADM	ADMC	ADMD	AJ	AJAC	AJAR	AJRO	ATT	B1	C1	COMP	DEF	DIF	NAR	
45	ABI	ABJ	ADAC	ADM	ADMC	ADMD	AIR	AJ	AJAR	AJCP	AJRO	AJS	ATT	B1	B2	C1	COMP	CRED	DEF	DES	DETT	DIF	ENER	ENRA	EXC	INFA	INFO	INST	LC	MACS	N1	N2	NAR	NON	NSUI	NVAL	OUI	SUIV	SUPS	TELE	TOEF	TOIE	VAL	VALC	VALR	
10	ABI	ABJ	ADMC	COMP	DEF	DIS	NVAL	VAL	VALC	VALR	
AB1 : Ajourné en B2 mais admis en B1	AB2 : ADMIS en B1 mais ajourné en B2	ABI : Absence	ABJ : Absence justifiée	ADM : Admis	AJ : Ajourné	AJRO : Ajourné - Réorientation Obligatoire	C1 : Niveau C1	DEF : Défaillant	DIF : Décision différée	
AB1 : Ajourné en B2 mais admis en B1	AB2 : ADMIS en B1 mais ajourné en B2	ABI : Absence	ABJ : Absence justifiée	ADM : Admis	ADMC : Admis avec compensation	ADMD : Admis (passage avec dette)	AJ : Ajourné	AJAC : Ajourné mais accès autorisé à étape sup.	AJAR : Ajourné et Admis A Redoubler	AJRO : Ajourné - Réorientation Obligatoire	ATT : En attente de décison	B1 : Niveau B1	C1 : Niveau C1	COMP : Compensé	DEF : Défaillant	DIF : Décision différée	NAR : Ajourné non admis à redoubler	
ABI : Absence	ABJ : Absence justifiée	ADAC : Admis avant choix	ADM : Admis	ADMC : Admis avec compensation	ADMD : Admis (passage avec dette)	AIR : Ingénieur spécialité Informatique appr	AJ : Ajourné	AJAR : Ajourné et Admis A Redoubler	AJCP : Ajourné mais autorisé à compenser	AJRO : Ajourné - Réorientation Obligatoire	AJS : Ajourné (note éliminatoire)	ATT : En attente de décison	B1 : Niveau B1	B2 : Niveau B2	C1 : Niveau C1	COMP : Compensé	CRED : Eléments en crédits	DEF : Défaillant	DES : Désistement	DETT : Eléments en dettes	DIF : Décision différée	ENER : Ingénieur spécialité Energétique	ENRA : Ingénieur spécialité Energétique appr	EXC : Exclu	INFA : Ingénieur spécialité Informatique appr	INFO : Ingénieur spécialié Informatique	INST : Ingénieur spécialité Instrumentation	LC : Liste complémentaire	MACS : Ingénieur spécialité MACS	N1 : Compétences CLES	N2 : Niveau N2	NAR : Ajourné non admis à redoubler	NON : Non	NSUI : Non suivi(e)	NVAL : Non Validé(e)	OUI : Oui	SUIV : Suivi(e)	SUPS : Supérieur au seuil	TELE : Ingénieur spéciailté Télécommunications	TOEF : TOEFL	TOIE : TOEIC	VAL : Validé(e)	VALC : Validé(e) par compensation	VALR : Validé(e) Retrospectivement	
ABI : Absence	ABJ : Absence justifiée	ADMC : Admis avec compensation	COMP : Compensé	DEF : Défaillant	DIS : Dispense examen	NVAL : Non Validé(e)	VAL : Validé(e)	VALC : Validé(e) par compensation	VALR : Validé(e) Retrospectivement	
							
XX-APO_COLONNES-XX
apoL_a01_code	Type Objet	Code	Version	Année	Session	Admission/Admissibilité	Type Rés.			Etudiant	Numéro
apoL_a02_nom											Nom
apoL_a03_prenom											Prénom
apoL_a04_naissance									Session	Admissibilité	Naissance
APO_COL_VAL_DEB
apoL_c0001	ELP	V1RETU21		2021	0	1	N	V1RETU21 - UE 2.1 Administrer réseau	0	1	Note
apoL_c0002	ELP	V1RETU21		2021	0	1	B		0	1	Barème
apoL_c0003	ELP	V1RETU21		2021	0	1	J		0	1	Pts Jury
apoL_c0004	ELP	V1RETU21		2021	0	1	R		0	1	Résultat
apoL_c0005	ELP	V1RETU22		2021	0	1	N	V1RETU22 - UE 2.2 Connecter entrep.	0	1	Note
apoL_c0006	ELP	V1RETU22		2021	0	1	B		0	1	Barème
apoL_c0007	ELP	V1RETU22		2021	0	1	J		0	1	Pts Jury
apoL_c0008	ELP	V1RETU22		2021	0	1	R		0	1	Résultat
apoL_c0009	ELP	V1RETU23		2021	0	1	N	V1RETU23 - UE 2.3 Créer des outils	0	1	Note
apoL_c0010	ELP	V1RETU23		2021	0	1	B		0	1	Barème
apoL_c0011	ELP	V1RETU23		2021	0	1	J		0	1	Pts Jury
apoL_c0012	ELP	V1RETU23		2021	0	1	R		0	1	Résultat
apoL_c0013	ELP	VRETR201		2021	0	1	N	VRETR201 - Technologie internet	0	1	Note
apoL_c0014	ELP	VRETR201		2021	0	1	B		0	1	Barème
apoL_c0015	ELP	VRETR202		2021	0	1	N	VRETR202 - Administration système	0	1	Note
apoL_c0016	ELP	VRETR202		2021	0	1	B		0	1	Barème
apoL_c0017	ELP	VRETR203		2021	0	1	N	VRETR203 - Bases service réseaux	0	1	Note
apoL_c0018	ELP	VRETR203		2021	0	1	B		0	1	Barème
apoL_c0019	ELP	VRETR204		2021	0	1	N	VRETR204 - Initiation téléphonie	0	1	Note
apoL_c0020	ELP	VRETR204		2021	0	1	B		0	1	Barème
apoL_c0021	ELP	VRETR205		2021	0	1	N	VRETR205 - Signaux et systèmes	0	1	Note
apoL_c0022	ELP	VRETR205		2021	0	1	B		0	1	Barème
apoL_c0023	ELP	VRETR206		2021	0	1	N	VRETR206 - Numérisation information	0	1	Note
apoL_c0024	ELP	VRETR206		2021	0	1	B		0	1	Barème
apoL_c0025	ELP	VRETR207		2021	0	1	N	VRETR207 - Sources de données	0	1	Note
apoL_c0026	ELP	VRETR207		2021	0	1	B		0	1	Barème
apoL_c0027	ELP	VRETR208		2021	0	1	N	VRETR208 - Analyse et traitement	0	1	Note
apoL_c0028	ELP	VRETR208		2021	0	1	B		0	1	Barème
apoL_c0029	ELP	VRETR209		2021	0	1	N	VRETR209 - Initiation au dév.	0	1	Note
apoL_c0030	ELP	VRETR209		2021	0	1	B		0	1	Barème
apoL_c0031	ELP	VRETR210		2021	0	1	N	VRETR210 - Anglais de communication	0	1	Note
apoL_c0032	ELP	VRETR210		2021	0	1	B		0	1	Barème
apoL_c0033	ELP	VRETR211		2021	0	1	N	VRETR211 - Exp., culture, com. pro2	0	1	Note
apoL_c0034	ELP	VRETR211		2021	0	1	B		0	1	Barème
apoL_c0035	ELP	VRETR212		2021	0	1	N	VRETR212 - PPP	0	1	Note
apoL_c0036	ELP	VRETR212		2021	0	1	B		0	1	Barème
apoL_c0037	ELP	VRETR213		2021	0	1	N	VRETR213 - Maths des syst. num	0	1	Note
apoL_c0038	ELP	VRETR213		2021	0	1	B		0	1	Barème
apoL_c0039	ELP	VRETR214		2021	0	1	N	VRETR214 - Analyse mathématique	0	1	Note
apoL_c0040	ELP	VRETR214		2021	0	1	B		0	1	Barème
apoL_c0041	ELP	VRETS21		2021	0	1	N	VRETS21 - Construire un réseau	0	1	Note
apoL_c0042	ELP	VRETS21		2021	0	1	B		0	1	Barème
apoL_c0043	ELP	VRETS22		2021	0	1	N	VRETS22 - Mesurer et car. un signal	0	1	Note
apoL_c0044	ELP	VRETS22		2021	0	1	B		0	1	Barème
apoL_c0045	ELP	VRETS23		2021	0	1	N	VRETS23 - Mettre en place une solut	0	1	Note
apoL_c0046	ELP	VRETS23		2021	0	1	B		0	1	Barème
apoL_c0047	ELP	VRETS24		2021	0	1	N	VRETS24 - Projet intégratif	0	1	Note
apoL_c0048	ELP	VRETS24		2021	0	1	B		0	1	Barème
apoL_c0049	ELP	VRETS25		2021	0	1	N	VRETS25 - Portfolio	0	1	Note
apoL_c0050	ELP	VRETS25		2021	0	1	B		0	1	Barème
apoL_c0051	ELP	V1RETW2		2021	0	1	N	V1RETW2 - Semestre 2 BUT RT 2	0	1	Note
apoL_c0052	ELP	V1RETW2		2021	0	1	B		0	1	Barème
apoL_c0053	ELP	V1RETW2		2021	0	1	J		0	1	Pts Jury
apoL_c0054	ELP	V1RETW2		2021	0	1	R		0	1	Résultat
apoL_c0055	ELP	V1RETO		2021	0	1	N	V1RETO - Année BUT 1 RT	0	1	Note
apoL_c0056	ELP	V1RETO		2021	0	1	B		0	1	Barème
apoL_c0057	VET	V1RET	117	2021	0	1	N	V1RET - BUT RT an1	0	1	Note
apoL_c0058	VET	V1RET	117	2021	0	1	B		0	1	Barème
apoL_c0059	VET	V1RET	117	2021	0	1	J		0	1	Pts Jury
apoL_c0060	VET	V1RET	117	2021	0	1	R		0	1	Résultat
APO_COL_VAL_FIN
apoL_c0061	APO_COL_VAL_FIN							  	
							
XX-APO_VALEURS-XX
apoL_a01_code	apoL_a02_nom	apoL_a03_prenom	apoL_a04_naissance	apoL_c0001	apoL_c0002	apoL_c0003	apoL_c0004	apoL_c0005	apoL_c0006	apoL_c0007	apoL_c0008	apoL_c0009	apoL_c0010	apoL_c0011	apoL_c0012	apoL_c0013	apoL_c0014	apoL_c0015	apoL_c0016	apoL_c0017	apoL_c0018	apoL_c0019	apoL_c0020	apoL_c0021	apoL_c0022	apoL_c0023	apoL_c0024	apoL_c0025	apoL_c0026	apoL_c0027	apoL_c0028	apoL_c0029	apoL_c0030	apoL_c0031	apoL_c0032	apoL_c0033	apoL_c0034	apoL_c0035	apoL_c0036	apoL_c0037	apoL_c0038	apoL_c0039	apoL_c0040	apoL_c0041	apoL_c0042	apoL_c0043	apoL_c0044	apoL_c0045	apoL_c0046	apoL_c0047	apoL_c0048	apoL_c0049	apoL_c0050	apoL_c0051	apoL_c0052	apoL_c0053	apoL_c0054	apoL_c0055	apoL_c0056	apoL_c0057	apoL_c0058	apoL_c0059	apoL_c0060	
12345678	INCONNU	ETUDIANT	 10/01/2003																																																												ADM
22345678	UN CONNU	ETUDIANT	 10/07/2003																																																												AJ
