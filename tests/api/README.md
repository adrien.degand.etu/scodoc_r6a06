# Tests unitaires de l'API ScoDoc

## Lancement des tests

La première fois, copier le fichier `tests/api/dotenv_exemple` vers
`tests/api/.env`. Il est normalement inutile de modifier son contenu.

Dans un shell, lancer le script `start_api_server.py`, qui se charge
d'initialiser une base SQL de test et de lancer le serveur ScoDoc approprié.

```bash
tests/api/start_api_server.sh
```

Dans un autre shell, lancer les tests:

```bash
pytest tests/api
```

## Notes sur la démarche

 1. On génère une base SQL de test: voir
   `tools/fakedatabase/create_test_api_database.py`

    1. En tant qu'utilisateur scodoc, lancer:

    ```bash
    # evite de modifier /opt/scodoc/.env
    export FLASK_ENV=test_api
    export FLASK_DEBUG=1 
    tools/create_database.sh --drop SCODOC_TEST_API
    flask db upgrade
    flask sco-db-init --erase
    flask init-test-database
    ```

    en plus court:

    ```bash
    export FLASK_ENV=test_api && tools/create_database.sh --drop SCODOC_TEST_API && flask db upgrade &&flask sco-db-init --erase && flask init-test-database
    ```

 2. On lance le serveur ScoDoc sur cette base

    ```bash
    flask run --host 0.0.0.0
    ```

 3. On lance les tests unitaires API

```bash
    pytest tests/api/test_api_departements.py
```

Rappel: pour interroger l'API, il fait avoir un utilisateur avec (au moins) la permission
ScoView dans tous les départements. Pour en créer un:

```bash
 flask user-create lecteur_api LecteurAPI @all
 flask user-password lecteur_api
 flask edit-role LecteurAPI -a ScoView
 flask user-role lecteur_api -a LecteurAPI
```
