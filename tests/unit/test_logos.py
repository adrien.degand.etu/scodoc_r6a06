# -*- coding: utf-8 -*-

"""Test Logos

Utiliser comme: 
    pytest tests/unit/test_logos.py
"""
from pathlib import Path
from shutil import copy

import pytest
from _pytest.python_api import approx

import app
import app.scodoc.sco_utils as scu
from app.scodoc.sco_logos import (
    find_logo,
    Logo,
    list_logos,
    write_logo,
    delete_logo,
)
from tests.unit.config_test_logos import create_dept, create_logos, LOGO_RESOURCES_DIR


def test_select_global_only(create_logos):
    dept1, dept2, dept3 = create_logos
    C_logo = app.scodoc.sco_logos.find_logo(logoname="C")
    assert C_logo.filepath == f"{scu.SCODOC_LOGOS_DIR}/logo_C.jpg"


def test_select_local_only(create_logos):
    dept1, dept2, dept3 = create_logos
    B_logo = app.scodoc.sco_logos.find_logo(logoname="B", dept_id=dept1.id)
    assert B_logo.filepath == f"{scu.SCODOC_LOGOS_DIR}/logos_{dept1.id}/logo_B.jpg"


def test_select_local_override_global(create_logos):
    dept1, dept2, dept3 = create_logos
    A1_logo = app.scodoc.sco_logos.find_logo(logoname="A", dept_id=dept1.id)
    assert A1_logo.filepath == f"{scu.SCODOC_LOGOS_DIR}/logos_{dept1.id}/logo_A.jpg"


def test_select_global_with_strict(create_logos):
    dept1, dept2, dept3 = create_logos
    A_logo = app.scodoc.sco_logos.find_logo(logoname="A", dept_id=dept1.id, strict=True)
    assert A_logo.filepath == f"{scu.SCODOC_LOGOS_DIR}/logos_{dept1.id}/logo_A.jpg"


def test_looks_for_non_existant_should_give_none(create_logos):
    # search for a local non-existant logo returns None
    dept1, dept2, dept3 = create_logos
    no_logo = app.scodoc.sco_logos.find_logo(logoname="Z", dept_id=dept1.id)
    assert no_logo is None


def test_looks_localy_for_a_global_should_give_none(create_logos):
    # search for a local non-existant logo returns None
    dept1, dept2, dept3 = create_logos
    no_logo = app.scodoc.sco_logos.find_logo(
        logoname="C", dept_id=dept1.id, strict=True
    )
    assert no_logo is None


def test_get_jpg_data(create_logos):
    dept1, dept2, dept3 = create_logos
    logo = find_logo("A", dept_id=None)
    assert logo is not None
    logo.select()
    assert logo.logoname == "A"
    assert logo.suffix == "jpg"
    assert logo.filename == "A.jpg"
    assert logo.size == (224, 131)
    assert logo.mm == approx((9.38, 5.49), 0.1)


def test_get_png_without_data(create_logos):
    dept1, dept2, dept3 = create_logos
    logo = find_logo("D", dept_id=None)
    assert logo is not None
    logo.select()
    assert logo.logoname == "D"
    assert logo.suffix == "png"
    assert logo.filename == "D.png"
    assert logo.size == (140, 131)
    assert logo.density is None
    assert logo.mm is None


def test_delete_unique_global_jpg_logo(create_logos):
    dept1, dept2, dept3 = create_logos
    from_path = Path(LOGO_RESOURCES_DIR).joinpath("logo_A.jpg")
    to_path = Path(scu.SCODOC_LOGOS_DIR).joinpath("logo_W.jpg")
    copy(from_path.absolute(), to_path.absolute())
    assert to_path.exists()
    delete_logo(name="W")
    assert not to_path.exists()


def test_delete_unique_local_jpg_logo(create_logos):
    dept1, dept2, dept3 = create_logos
    from_path = Path(LOGO_RESOURCES_DIR).joinpath("logo_A.jpg")
    to_path = Path(scu.SCODOC_LOGOS_DIR).joinpath(f"logos_{dept1.id}", "logo_W.jpg")
    copy(from_path.absolute(), to_path.absolute())
    assert to_path.exists()
    delete_logo(name="W", dept_id=dept1.id)
    assert not to_path.exists()


def test_delete_multiple_local_jpg_logo(create_logos):
    dept1, dept2, dept3 = create_logos
    from_path_A = Path(LOGO_RESOURCES_DIR).joinpath("logo_A.jpg")
    to_path_A = Path(scu.SCODOC_LOGOS_DIR).joinpath(f"logos_{dept1.id}", "logo_V.jpg")
    from_path_B = Path(LOGO_RESOURCES_DIR).joinpath("logo_D.png")
    to_path_B = Path(scu.SCODOC_LOGOS_DIR).joinpath(f"logos_{dept1.id}", "logo_V.png")
    copy(from_path_A.absolute(), to_path_A.absolute())
    copy(from_path_B.absolute(), to_path_B.absolute())
    assert to_path_A.exists()
    assert to_path_B.exists()
    delete_logo(name="V", dept_id=dept1.id)
    assert not to_path_A.exists()
    assert not to_path_B.exists()


def test_create_global_jpg_logo(create_logos):
    dept1, dept2, dept3 = create_logos
    path = Path(f"{LOGO_RESOURCES_DIR}/logo_C.jpg")
    stream = path.open("rb")
    logo_path = Path(scu.SCODOC_LOGOS_DIR).joinpath("logo_X.jpg")
    assert not logo_path.exists()
    write_logo(stream, name="X")  # create global logo
    assert logo_path.exists()
    logo_path.unlink(missing_ok=True)


def test_create_locale_jpg_logo(create_logos):
    dept1, dept2, dept3 = create_logos
    path = Path(f"{LOGO_RESOURCES_DIR}/logo_C.jpg")
    stream = path.open("rb")
    logo_path = Path(scu.SCODOC_LOGOS_DIR).joinpath(f"logos_{dept1.id}", "logo_Y.jpg")
    assert not logo_path.exists()
    write_logo(stream, name="Y", dept_id=dept1.id)  # create global logo
    assert logo_path.exists()
    logo_path.unlink(missing_ok=True)


def test_create_jpg_instead_of_png_logo(create_logos):
    dept1, dept2, dept3 = create_logos
    # action
    logo = Logo("D")  # create global logo (replace logo_D.png)
    path = Path(f"{LOGO_RESOURCES_DIR}/logo_C.jpg")
    stream = path.open("rb")
    logo.create(stream)
    # test
    created = Path(f"{scu.SCODOC_LOGOS_DIR}/logo_D.jpg")
    removed = Path(f"{scu.SCODOC_LOGOS_DIR}/logo_D.png")
    # file system check
    assert created.exists()
    assert not removed.exists()
    # logo check
    logo = find_logo("D")
    assert logo is not None
    assert logo.filepath == f"{scu.SCODOC_LOGOS_DIR}/logo_D.jpg"  # created.absolute()
    # restore initial state
    original = Path(f"{LOGO_RESOURCES_DIR}/logo_D.png")
    copy(original, removed)
    created.unlink(missing_ok=True)


@pytest.mark.skip(
    reason="il peut y avoir d'autres fichiers logos sur les serveurs de dev"
)
def test_list_logo(create_logos):
    """Vérifie que nos logos sont présents et que ce sont les seuls."""
    dept1, dept2, dept3 = create_logos
    logos = list_logos()
    assert set(logos.keys()) == {dept1.id, dept2.id, None}
    assert {"A", "C", "D", "E", "F", "header", "footer"}.issubset(
        set(logos[None].keys())
    )
    rt = logos.get(dept1.id, None)
    assert rt is not None
    assert {"A", "B"}.issubset(set(rt.keys()))
    info = logos.get(dept2.id, None)
    assert info is not None
    assert {"A"}.issubset(set(rt.keys()))
    gea = logos.get(dept3.id, None)
    assert gea is None
