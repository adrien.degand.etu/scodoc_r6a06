# -*- coding: UTF-8 -*

"""Unit tests : cursus_but


Ce test suppose une base département existante.

Usage: pytest tests/unit/test_but_cursus.py
"""


import pytest
from tests.unit import yaml_setup

import app

# XXX from app.but.cursus_but import FormSemestreCursusBUT
from app.comp import res_sem
from app.comp.res_but import ResultatsSemestreBUT
from app.models import ApcParcours, ApcReferentielCompetences, FormSemestre
from config import TestConfig

DEPT = TestConfig.DEPT_TEST


@pytest.mark.skip  # XXX WIP
@pytest.mark.slow
def test_cursus_but_jury_gb(test_client):
    # Construit la base de test GB une seule fois
    # puis lance les tests de jury
    app.set_sco_dept(DEPT)
    # login_user(User.query.filter_by(user_name="admin").first()) # XXX pour tests manuels
    # ctx.push() # XXX
    doc, formation, formsemestre_titres = yaml_setup.setup_from_yaml(
        "tests/ressources/yaml/cursus_but_gb.yaml"
    )
    for formsemestre_titre in formsemestre_titres:
        formsemestre = yaml_setup.create_formsemestre_with_etuds(
            doc, formation, formsemestre_titre
        )
    formsemestre: FormSemestre = FormSemestre.query.filter_by(titre="S3").first()
    res: ResultatsSemestreBUT = res_sem.load_formsemestre_results(formsemestre)
    cursus = FormSemestreCursusBUT(res)
    ref_comp: ApcReferentielCompetences = formsemestre.formation.referentiel_competence
    # Vérifie niveaux du tronc commun:
    niveaux_parcours_by_annee_tc = cursus.get_niveaux_parcours_by_annee(None)
    assert set(niveaux_parcours_by_annee_tc.keys()) == {1, 2, 3}
    # structure particulière à GB:
    assert [len(niveaux_parcours_by_annee_tc[annee]) for annee in (1, 2, 3)] == [
        2,
        2,
        1,
    ]
    parcour: ApcParcours = ref_comp.parcours.filter_by(code="SEE").first()
    assert parcour
    niveaux_parcours_by_annee_see = cursus.get_niveaux_parcours_by_annee(parcour)
    assert set(niveaux_parcours_by_annee_see.keys()) == {1, 2, 3}
    # GB SEE: 4 niveaux en BU1, 5 en BUT2, 4 en BUT3
    assert [len(niveaux_parcours_by_annee_see[annee]) for annee in (1, 2, 3)] == [
        4,
        5,
        4,
    ]
    # Un étudiant inscrit en SEE
    inscription = formsemestre.etuds_inscriptions[1]
    assert inscription.parcour.code == "SEE"
    etud = inscription.etud
    assert cursus.get_niveaux_parcours_etud(etud) == niveaux_parcours_by_annee_see


# @pytest.mark.skip  # XXX WIP
@pytest.mark.slow
def test_refcomp_niveaux_info(test_client):
    """Test niveaux / parcours / UE pour un BUT INFO
    avec parcours A et C, même compétences mais coefs différents
    selon le parcours.
    """
    # WIP
    # pour le moment juste le chargement de la formation, du ref. comp, et des UE du S4.
    app.set_sco_dept(DEPT)
    doc, formation, formsemestre_titres = yaml_setup.setup_from_yaml(
        "tests/ressources/yaml/cursus_but_info.yaml"
    )
    for formsemestre_titre in formsemestre_titres:
        formsemestre = yaml_setup.create_formsemestre_with_etuds(
            doc, formation, formsemestre_titre
        )
    formsemestre: FormSemestre = FormSemestre.query.filter_by(titre="S4").first()
    assert formsemestre
    res: ResultatsSemestreBUT = res_sem.load_formsemestre_results(formsemestre)
