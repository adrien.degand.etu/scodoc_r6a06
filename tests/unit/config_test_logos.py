# -*- coding: utf-8 -*-

"""Test Logos
    Mise en place de l'environnement de test pour logos
"""
import os
from pathlib import Path
from shutil import copytree, rmtree, copy

import pytest

import app.scodoc.sco_utils as scu
from app import db, Departement
from app.auth.models import User, Role
from config import TestConfig
from scodoc import app
from tests.conftest import test_client, RESOURCES_DIR

LOGO_RESOURCES_DIR = os.path.join(RESOURCES_DIR, "test_logos")


@pytest.fixture
def create_dept(test_client):
    """Crée 3 départements:
    return departements object
    """
    client = test_client
    dept1 = Departement(acronym="RT")
    dept2 = Departement(acronym="INFO")
    dept3 = Departement(acronym="GEA")
    db.session.add(dept1)
    db.session.add(dept2)
    db.session.add(dept3)
    db.session.commit()
    yield dept1, dept2, dept3
    db.session.delete(dept1)
    db.session.delete(dept2)
    db.session.delete(dept3)
    db.session.commit()


@pytest.fixture
def create_logos(create_dept):
    """Crée les logos:
    ...logos --+-- logo_A.jpg
               +-- logo_C.jpg
               +-- logo_D.png
               +-- logo_E.jpg
               +-- logo_F.jpeg
               +-- logos_{d1} --+-- logo_A.jpg
               |                +-- logo_B.jpg
               +-- logos_{d2} --+-- logo_A.jpg

    """
    dept1, dept2, dept3 = create_dept
    dept1_id = dept1.id
    dept2_id = dept2.id
    FILE_LIST = ["logo_A.jpg", "logo_C.jpg", "logo_D.png", "logo_E.jpg", "logo_F.jpeg"]
    for filename in FILE_LIST:
        from_path = Path(LOGO_RESOURCES_DIR).joinpath(filename)
        to_path = Path(scu.SCODOC_LOGOS_DIR).joinpath(filename)
        copy(from_path.absolute(), to_path.absolute())
    copytree(
        f"{LOGO_RESOURCES_DIR}/logos_1",
        f"{scu.SCODOC_LOGOS_DIR}/logos_{dept1_id}",
    )
    copytree(
        f"{LOGO_RESOURCES_DIR}/logos_2",
        f"{scu.SCODOC_LOGOS_DIR}/logos_{dept2_id}",
    )
    yield dept1, dept2, dept3
    rmtree(f"{scu.SCODOC_LOGOS_DIR}/logos_{dept1_id}")
    rmtree(f"{scu.SCODOC_LOGOS_DIR}/logos_{dept2_id}")
    # rm files
    for filename in FILE_LIST:
        to_path = Path(scu.SCODOC_LOGOS_DIR).joinpath(filename)
        to_path.unlink()


def get_token(user_name):
    with app.test_client() as client:
        response = client.post("/ScoDoc/api/tokens", auth=(user_name, user_name))
        assert response.status_code == 200
        token = response.json["token"]
        user = User.check_token(token) if token else None
        assert user.user_name == user_name
        app.logger.info(f"{user.user_name}: token obtained: {token}")
        return token


@pytest.fixture
def create_super_token(create_logos):
    dept1, dept2, dept3 = create_logos
    # change super_admin password
    # utilisateur mozart -> super
    user_name = "mozart"
    u = User.query.filter_by(user_name=user_name).first()
    if u is None:
        u = User(user_name=user_name)
    u.set_password(user_name)
    if "SuperAdmin" not in {r.name for r in u.roles}:
        super_role = Role.query.filter_by(name="SuperAdmin").first()
        u.add_role(super_role, None)
    db.session.add(u)
    db.session.commit()
    return dept1, dept2, dept3, get_token(u.user_name)


@pytest.fixture
def create_admin_token(create_logos):
    dept1, dept2, dept3 = create_logos
    # utilisateur bach -> admin
    user_name = "bach"
    u = User.query.filter_by(user_name=user_name).first()
    if u is None:
        u = User(user_name=user_name)
    u.set_password(user_name)
    if "Admin" not in {r.name for r in u.roles}:
        admin_role = Role.query.filter_by(name=user_name).first()
        u.add_role(admin_role, TestConfig.DEPT_TEST)
    db.session.add(u)
    db.session.commit()
    return dept1, dept2, dept3, get_token(u.user_name)


@pytest.fixture
def create_lambda_token(create_logos):
    dept1, dept2, dept3 = create_logos
    # utilisateur vivaldi -> lambda
    user_name = "vivaldi"
    u = User.query.filter_by(user_name=user_name).first()
    if u is None:
        u = User(user_name=user_name)
    u.set_password(user_name)
    db.session.add(u)
    db.session.commit()
    return dept1, dept2, dept3, get_token(user_name)
