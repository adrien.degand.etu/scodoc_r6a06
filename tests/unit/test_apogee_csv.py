##############################################################################
# ScoDoc
# Copyright (c) 1999 - 2024 Emmanuel Viennet.  All rights reserved.
# See LICENSE
##############################################################################

""" Test lecture/érciture fichiers Apogée

"""

import pytest
from flask import g

import app
from app import db
from app.models import Formation, FormSemestreEtape
from app.scodoc import sco_apogee_csv, sco_apogee_reader
from config import TestConfig
from tests.conftest import RESOURCES_DIR
from tests.unit import yaml_setup


DEPT = TestConfig.DEPT_TEST
APO_CSV_FILE = RESOURCES_DIR + "/apogee/V1RET!117.txt"


def test_apogee_csv(test_client):
    """Lecture/écriture d'un fichier Apogée: vérifie que le fichier écrit est
    strictement identique au fichier lu.
    (le semestre n'ayant aucun résultat)
    """
    app.set_sco_dept(DEPT)
    # Met en place une formation et un semestre
    formation = Formation(
        dept_id=g.scodoc_dept_id,
        acronyme="TESTAPO",
        titre="Test Apo",
        titre_officiel="Test Apof",
    )
    db.session.add(formation)
    formsemestre = yaml_setup.create_formsemestre(
        formation, [], 1, "S1_apo", "2021-09-01", "2022-01-15"
    )
    etape = FormSemestreEtape(etape_apo="V1RET!117")
    formsemestre.etapes.append(etape)
    db.session.add(formsemestre)
    db.session.commit()
    #
    with open(APO_CSV_FILE, encoding=sco_apogee_reader.APO_INPUT_ENCODING) as f:
        data = f.read()
    assert "ETUDIANT" in data
    #
    apo_data = sco_apogee_csv.ApoData(data, periode=2)
    apo_data.setup()
    assert len(apo_data.apo_csv.csv_etuds) == 2
    apo_etuds = apo_data.etud_by_nip.values()
    for apo_etud in apo_etuds:
        apo_etud.is_apc = apo_data.is_apc
        apo_etud.lookup_scodoc(apo_data.etape_formsemestre_ids)
        apo_etud.associate_sco(apo_data)
    data_2 = (
        apo_data.apo_csv.write(apo_etuds)
        .decode(sco_apogee_reader.APO_INPUT_ENCODING)
        .replace("\r", "")
    )
    # open("toto.txt", "w", encoding=sco_apogee_reader.APO_INPUT_ENCODING).write(data_2)
    assert data == data_2
