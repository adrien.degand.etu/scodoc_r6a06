# -*- coding: utf-8 -*-

##############################################################################
#
# Gestion scolarite IUT
#
# Copyright (c) 1999 - 2022 Emmanuel Viennet.  All rights reserved.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#   Emmanuel Viennet      emmanuel.viennet@viennet.net
#
##############################################################################

"""ScoDoc : gestion des fichiers archivés associés aux assiduités
     Il s'agit de fichiers quelconques, xxx
"""

# Exemple minimal pour @iziram

from app.scodoc import sco_archives


class AbsArchiver(sco_archives.BaseArchiver):
    def __init__(self):
        sco_archives.BaseArchiver.__init__(
            self, archive_type="xp_abs"
        )  # <<< adapter le nom du type pour refléter l'usage


AbsArchive = AbsArchiver()

# A partir d'ici, essais interactifs avec flask shell

mapp.set_sco_dept("RT")
ctx.push()

# supposons que l'id de l'archive soit l'étudid
# (cela pourrait être autre chose, comme un id d'assiduité ou de justif...)

mon_id = 1
AbsArchive.list_obj_archives(mon_id)

"""
On voit que ScoDoc s'occupe de créer les répertoires côté serveur
[Mon Dec 26 19:38:11 2022] scodoc:  (RT) initialized archiver, path=/opt/scodoc-data/archives/xp_abs
[Mon Dec 26 19:38:11 2022] scodoc:  (RT) creating directory /opt/scodoc-data/archives/xp_abs
[Mon Dec 26 19:38:11 2022] scodoc:  (RT) creating directory /opt/scodoc-data/archives/xp_abs/5
[Mon Dec 26 19:38:11 2022] scodoc:  (RT) creating directory /opt/scodoc-data/archives/xp_abs/5/1
[]

et a renvoyé une liste vide
"""

archive_id = AbsArchive.create_obj_archive(
    mon_id, "la description du truc que je veux archiver"
)

data = b"un paquet de donnees a stocker"

AbsArchive.store(archive_id, "toto", data)

AbsArchive.store(archive_id, "../../tmp/titi", data + data)
# ici on remarque que le chemin a été vérifié et sécurisé

# ------   Relecture:
archive_ids = AbsArchive.list_obj_archives(mon_id)
# -> ['/opt/scodoc-data/archives/xp_abs/5/1/2022-12-26-19-41-54']

assert archive_ids[0] == archive_id

AbsArchive.get_archive_description(archive_id)

# Liste le contenu:
names = AbsArchive.list_archive(archive_id)
assert ["....tmptiti", "toto"] == names

# Obtient un objet de l'archive
archive_name = AbsArchive.get_archive_name(archive_id)
data2 = AbsArchive.get_archived_file(mon_id, archive_name, names[0])

# Et enfin pour effacer une archive:
AbsArchive.delete_archive(mon_id)
