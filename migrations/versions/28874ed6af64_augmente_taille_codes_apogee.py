"""augmente taille codes Apogée

Revision ID: 28874ed6af64
Revises: f40fbaf5831c
Create Date: 2022-01-19 22:57:59.678313

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

# revision identifiers, used by Alembic.
revision = "28874ed6af64"
down_revision = "f40fbaf5831c"
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###

    op.alter_column(
        "notes_formsemestre_etapes",
        "etape_apo",
        existing_type=sa.VARCHAR(length=24),
        type_=sa.String(length=512),
        existing_nullable=True,
    )
    op.alter_column(
        "notes_formsemestre_inscription",
        "etape",
        existing_type=sa.VARCHAR(length=24),
        type_=sa.String(length=512),
        existing_nullable=True,
    )
    op.alter_column(
        "notes_modules",
        "code_apogee",
        existing_type=sa.VARCHAR(length=24),
        type_=sa.String(length=512),
        existing_nullable=True,
    )
    op.alter_column(
        "notes_ue",
        "code_apogee",
        existing_type=sa.VARCHAR(length=24),
        type_=sa.String(length=512),
        existing_nullable=True,
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.alter_column(
        "notes_ue",
        "code_apogee",
        existing_type=sa.String(length=512),
        type_=sa.VARCHAR(length=24),
        existing_nullable=True,
    )
    op.alter_column(
        "notes_modules",
        "code_apogee",
        existing_type=sa.String(length=512),
        type_=sa.VARCHAR(length=24),
        existing_nullable=True,
    )
    op.alter_column(
        "notes_formsemestre_inscription",
        "etape",
        existing_type=sa.String(length=512),
        type_=sa.VARCHAR(length=24),
        existing_nullable=True,
    )
    op.alter_column(
        "notes_formsemestre_etapes",
        "etape_apo",
        existing_type=sa.String(length=512),
        type_=sa.VARCHAR(length=24),
        existing_nullable=True,
    )

    # ### end Alembic commands ###
