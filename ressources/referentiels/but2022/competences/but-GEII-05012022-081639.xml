<?xml version="1.0" encoding="UTF-8"?>
<referentiel_competence specialite="GEII"
                        specialite_long="Génie électrique et informatique industrielle"
                        type="B.U.T." annexe="8"
                        type_structure="type1"
                        type_departement="secondaire"
                        version="2021-12-11 00:00:00"
>
    <competences>
                <competence nom_court="Concevoir"
                    numero="1"
                    libelle_long="Concevoir la partie GEII d’un système"
                    couleur="c1"
                    id="b078d2f7f61c73d3d3c47a1b01b0ba4d">
            <situations>
                                <situation>Conseil au client en menant une étude de faisabilité à partir d&#039;un cahier des charges
                </situation>
                                <situation>Chiffrage pour la réalisation d&#039;un prototype ou d’un système industriel en GEII</situation>
                                <situation>Conception d’un prototype ou d’un sous système à partir d&#039;un cahier des charges partiel
                </situation>
                            </situations>
            <composantes_essentielles>
                                <composante>En adoptant une approche holistique intégrant les innovations technologiques en lien avec la
                    stratégie de l’entreprise pour répondre un besoin client.
                </composante>
                                <composante>En produisant l’ensemble des documents nécessaires pour le client et les différents
                    prestataires
                </composante>
                                <composante>En communiquant de façon adaptée avec les différents acteurs avant et pendant la phase de
                    conception.
                </composante>
                            </composantes_essentielles>
            <niveaux>
                                <niveau ordre="1" libelle="Mener une conception partielle intégrant une démarche projet " annee="BUT1">
                <acs>
                                        <ac code="AC11.01">Produire une analyse fonctionnelle d’un système simple
                        </ac>
                                        <ac code="AC11.02">Réaliser un prototype pour des solutions techniques matériel et/ou logiciel
                        </ac>
                                        <ac code="AC11.03">Rédiger un dossier de fabrication à partir d&#039;un dossier de conception
                        </ac>
                                    </acs>
                </niveau>
                                <niveau ordre="2" libelle="Concevoir un système en fiabilisant les solutions propoées" annee="BUT2">
                <acs>
                                        <ac code="AC21.01">Proposer des solutions techniques liées à l&#039;analyse fonctionnelle</ac>
                                        <ac code="AC21.02">Dérisquer les solutions techniques retenues</ac>
                                    </acs>
                </niveau>
                                <niveau ordre="3" libelle="Concevoir un système en adoptant une approche sélective dans ses choix technologiques" annee="BUT3">
                <acs>
                                        <ac code="AC31.01">Contribuer à la rédaction d&#039;un cahier des charges</ac>
                                        <ac code="AC31.02">Prouver la pertinence de ses choix technologiques</ac>
                                        <ac code="AC31.03">Rédiger un dossier de conception</ac>
                                    </acs>
                </niveau>
                            </niveaux>
        </competence>
                <competence nom_court="Vérifier"
                    numero="2"
                    libelle_long="Vérifier la partie GEII d’un système"
                    couleur="c2"
                    id="8d6ba092510bfa04de81c140fc69816d">
            <situations>
                                <situation>Mise en place d&#039;un protocole de tests et de mesures dans les domaines de la gestion,
                    production et maîtrise de l’énergie
                </situation>
                                <situation>Mise en place d&#039;un protocole de tests et de mesures dans les process industriels</situation>
                                <situation>Mise en place d&#039;un protocole de tests et de mesures dans les systèmes embarqués</situation>
                            </situations>
            <composantes_essentielles>
                                <composante>En tenant compte des spécificités matérielles, réglementaires et contextuelles</composante>
                                <composante>En mettant en oeuvre un plan d’essais et d’évaluations, dans une visée d&#039;analyse qualitative
                    et corrective
                </composante>
                                <composante>En tenant compte des enjeux économiques, environnementaux et réglementaires de la société
                </composante>
                            </composantes_essentielles>
            <niveaux>
                                <niveau ordre="1" libelle="Effectuer les tests et mesures nécessaires à une vérification d’un système" annee="BUT1">
                <acs>
                                        <ac code="AC12.01">Appliquer une procédure d’essais</ac>
                                        <ac code="AC12.02">Identifier un dysfonctionnement</ac>
                                        <ac code="AC12.03">Décrire un dysfonctionnement</ac>
                                    </acs>
                </niveau>
                                <niveau ordre="2" libelle="Mettre en place un protocole de tests pour valider le fonctionnement d’un système" annee="BUT2">
                <acs>
                                        <ac code="AC22.01">Identifier les tests et mesures à mettre en place pour valider le
                            fonctionnement d’un système
                        </ac>
                                        <ac code="AC22.02">Certifier le fonctionnement d’un nouvel équipement industriel</ac>
                                    </acs>
                </niveau>
                                <niveau ordre="3" libelle="Élaborer une procédure intégrant une démarche qualité pour valider le fonctionnement d’un système" annee="BUT3">
                <acs>
                                        <ac code="AC32.01">Evaluer la cause racine d’un dysfonctionnement</ac>
                                        <ac code="AC32.02">Proposer une solution corrective à un dysfonctionnement</ac>
                                        <ac code="AC32.03">Produire une procédure d’essais pour valider la conformité d’un système</ac>
                                    </acs>
                </niveau>
                            </niveaux>
        </competence>
                <competence nom_court="Maintenir"
                    numero="3"
                    libelle_long="Assurer le maintien en condition opérationnelle d&#039;un système"
                    couleur="c3"
                    id="a1ca5c95c20b946c22eb8a1ce1047359">
            <situations>
                                <situation>Maintenance corrective, préventive et améliorative dans les domaines de la gestion,
                    production et maîtrise de l’énergie
                </situation>
                                <situation>Maintenance corrective, préventive et améliorative dans les process industriels
                </situation>
                                <situation>Maintenance corrective, préventive et améliorative dans les systèmes embarqués
                </situation>
                            </situations>
            <composantes_essentielles>
                                <composante>En adoptant une communication proactive avec les différents acteurs</composante>
                                <composante>En adoptant une approche holistique intégrant les nouvelles technologies et la
                    transformation digitale
                </composante>
                            </composantes_essentielles>
            <niveaux>
                                <niveau ordre="1" libelle="Intervenir sur un système pour effectuer une opération de maintenance" annee="BUT2">
                <acs>
                                        <ac code="AC23.01">Exécuter l’entretien et le contrôle d’un système en respectant une procédure
                        </ac>
                                        <ac code="AC23.02">Exécuter une opération de maintenance (corrective, préventive, améliorative)
                        </ac>
                                        <ac code="AC23.03">Diagnostiquer un dysfonctionnement dans un système</ac>
                                        <ac code="AC23.04">Identifier la cause racine du dysfonctionnement</ac>
                                    </acs>
                </niveau>
                                <niveau ordre="2" libelle="Mettre en place une stratégie de maintenance pour garantir un fonctionnement optimal" annee="BUT3">
                <acs>
                                        <ac code="AC33.01">Proposer une solution de maintenance</ac>
                                        <ac code="AC33.02">Évaluer les coûts d’indisponibilité et de maintenance d’un système</ac>
                                        <ac code="AC33.03">Produire une procédure de maintenance</ac>
                                        <ac code="AC33.03">Proposer un appui technique aux différents acteurs à l&#039;échelle nationale et
                            internationale
                        </ac>
                                    </acs>
                </niveau>
                            </niveaux>
        </competence>
                <competence nom_court="Implanter"
                    numero="4"
                    libelle_long="Implanter un système matériel ou logiciel"
                    couleur="c4"
                    id="28d79046d48712bcb7addd96d8244104">
            <situations>
                                <situation>Homologation d’un protocole de réalisation pour un nouvel équipement industriel
                </situation>
                                <situation>Intervention chez un client pour la mise en place d’un système
                </situation>
                                <situation>Implantation d’une solution matérielle ou logicielle dans une partie ou sous partie d’un
                    système
                </situation>
                            </situations>
            <composantes_essentielles>
                                <composante>En tenant compte des aspects organisationnels liés aux contextes industriels, humains et
                    environnementaux
                </composante>
                                <composante>En garantissant un livrable conforme aux dossiers de conception, de fabrication et des
                    normes
                </composante>
                                <composante>En garantissant un accompagnement client amont, aval et transverse dans une démarche
                    qualité
                </composante>
                            </composantes_essentielles>
            <niveaux>
                                <niveau ordre="1" libelle="Réaliser un système en mettant en place une démarche qualité en conformité avec le dossier de fabrication" annee="BUT2">
                <acs>
                                        <ac code="AC24.01ESE">Appliquer une procédure de fabrication pour implanter les composants matériels
                            et/ou logiciels dans un système
                        </ac>
                                        <ac code="AC24.02ESE">Évaluer la conformité du système</ac>
                                    </acs>
                </niveau>
                                <niveau ordre="2" libelle="Interagir avec les différents acteurs, lors de l’installation et de la mise en service d’un système, dans une démarche qualité" annee="BUT3">
                <acs>
                                        <ac code="AC34.01ESE">Produire une procédure d’installation et de mise en service d’un système
                        </ac>
                                        <ac code="AC34.02ESE">Exécuter la mise en service d’un système en respectant la procédure</ac>
                                        <ac code="AC34.03ESE">Produire le dossier de conformité du système en gérant le versionnage</ac>
                                    </acs>
                </niveau>
                            </niveaux>
        </competence>
                <competence nom_court="Installer"
                    numero="4"
                    libelle_long="Installer tout ou partie d’un système de production, de conversion et de gestion d’énergie"
                    couleur="c4"
                    id="124219a128a6c4bf99fc3c45dbf0bf55">
            <situations>
                                <situation>Planification d’opérations d’installation d’équipements industriels dans les domaines de
                    courants forts
                </situation>
                                <situation>Montage et installation d’éléments ou sous-ensembles d’un équipement industriel en
                    production, distribution, gestion ou conversion d’énergie
                </situation>
                                <situation>Mise en service d’un nouvel équipement industriel en production, distribution , gestion ou
                    conversion d’énergie
                </situation>
                                <situation>Étude de l’implantation d’un équipements de production, distribution , de gestion ou de
                    conversion d’énergie
                </situation>
                            </situations>
            <composantes_essentielles>
                                <composante>En garantissant un accompagnement client amont, aval et transverse dans une démarche
                    qualité
                </composante>
                                <composante>En respectant les normes et les contraintes réglementaires liées aux courants forts, y
                    compris dans un contexte international
                </composante>
                            </composantes_essentielles>
            <niveaux>
                                <niveau ordre="1" libelle="Procéder à une installation ou à une mise en service en suivant un protocole" annee="BUT2">
                <acs>
                                        <ac code="AC24.01EME">Appliquer la procédure d’installation d’un système</ac>
                                        <ac code="AC24.02EME">Exécuter la mise en service d’un système en respectant la procédure</ac>
                                    </acs>
                </niveau>
                                <niveau ordre="2" libelle="Interagir avec les différents acteurs, depuis l’élaboration du protocole jusqu’à l’installation, dans une visée de démarche qualité" annee="BUT3">
                <acs>
                                        <ac code="AC34.01EME">Planifier l’installation et la mise en service d’un nouvel équipement</ac>
                                        <ac code="AC34.02EME">Produire une procédure d’installation et de mise en service d’un système</ac>
                                        <ac code="AC34.03EME">Produire le dossier de conformité du système en gérant le versionnage</ac>
                                    </acs>
                </niveau>
                            </niveaux>
        </competence>
                <competence nom_court="Intégrer"
                    numero="4"
                    libelle_long="Intégrer un système de commande et de contrôle dans un procédé industriel"
                    couleur="c4"
                    id="022ed4cb9c2b145cc22bc9ac0b41a245">
            <situations>
                                <situation>Planification d’opérations d’installation d’un système automatisé et/ou d’une architecture
                    réseau
                </situation>
                                <situation>Montage et installation d’éléments ou sous-ensembles d’un système automatisé et/ou d’une
                    architecture réseau
                </situation>
                                <situation>Mise en service d’un système automatisé et/ou d’une architecture réseau</situation>
                                <situation>Étude d’implantation d’un système automatisé et/ou d’une architecture réseau dans un contexte
                    industriel
                </situation>
                            </situations>
            <composantes_essentielles>
                                <composante>En garantissant un accompagnement client amont, aval et transverse dans une démarche
                    qualité
                </composante>
                                <composante>En respectant les normes et les contraintes réglementaires y compris dans un contexte
                    international
                </composante>
                                <composante>En gérant les réseaux industriels de communication pour une meilleure disponibilité et
                    sécurité
                </composante>
                            </composantes_essentielles>
            <niveaux>
                                <niveau ordre="1" libelle="Procéder à une installation ou à une mise en service en suivant un protocole" annee="BUT2">
                <acs>
                                        <ac code="AC24.01AII">Appliquer la procédure d’installation d’un système</ac>
                                        <ac code="AC24.02AII">Exécuter la mise en service d’un système en respectant la procédure</ac>
                                    </acs>
                </niveau>
                                <niveau ordre="2" libelle="Interagir avec les différents acteurs, depuis l’élaboration du protocole jusqu’à l’installation, dans une visée de démarche qualité" annee="BUT3">
                <acs>
                                        <ac code="AC34.01AII">Planifier l’installation et la mise en service d’un nouvel équipement</ac>
                                        <ac code="AC34.02AII">Produire une procédure d’installation et de mise en service d’un système</ac>
                                        <ac code="AC34.03AII">Produire le dossier de conformité du système en gérant le versionnage</ac>
                                    </acs>
                </niveau>
                            </niveaux>
        </competence>
            </competences>
    <parcours>
                <parcour numero="0" libelle="Électronique et systèmes embarqués" code="ESE">
                        <annee ordre="1">
                                <competence niveau="1" id="b078d2f7f61c73d3d3c47a1b01b0ba4d"/>
                                <competence niveau="1" id="8d6ba092510bfa04de81c140fc69816d"/>
                            </annee>
                        <annee ordre="2">
                                <competence niveau="1" id="a1ca5c95c20b946c22eb8a1ce1047359"/>
                                <competence niveau="2" id="b078d2f7f61c73d3d3c47a1b01b0ba4d"/>
                                <competence niveau="2" id="8d6ba092510bfa04de81c140fc69816d"/>
                                <competence niveau="1" id="28d79046d48712bcb7addd96d8244104"/>
                            </annee>
                        <annee ordre="3">
                                <competence niveau="2" id="a1ca5c95c20b946c22eb8a1ce1047359"/>
                                <competence niveau="3" id="b078d2f7f61c73d3d3c47a1b01b0ba4d"/>
                                <competence niveau="3" id="8d6ba092510bfa04de81c140fc69816d"/>
                                <competence niveau="2" id="28d79046d48712bcb7addd96d8244104"/>
                            </annee>
                    </parcour>
                <parcour numero="0" libelle="Électricité et maîtrise de l&#039;énergie" code="EME">
                        <annee ordre="1">
                                <competence niveau="1" id="b078d2f7f61c73d3d3c47a1b01b0ba4d"/>
                                <competence niveau="1" id="8d6ba092510bfa04de81c140fc69816d"/>
                            </annee>
                        <annee ordre="2">
                                <competence niveau="1" id="a1ca5c95c20b946c22eb8a1ce1047359"/>
                                <competence niveau="2" id="b078d2f7f61c73d3d3c47a1b01b0ba4d"/>
                                <competence niveau="2" id="8d6ba092510bfa04de81c140fc69816d"/>
                                <competence niveau="1" id="124219a128a6c4bf99fc3c45dbf0bf55"/>
                            </annee>
                        <annee ordre="3">
                                <competence niveau="2" id="a1ca5c95c20b946c22eb8a1ce1047359"/>
                                <competence niveau="3" id="b078d2f7f61c73d3d3c47a1b01b0ba4d"/>
                                <competence niveau="3" id="8d6ba092510bfa04de81c140fc69816d"/>
                                <competence niveau="2" id="124219a128a6c4bf99fc3c45dbf0bf55"/>
                            </annee>
                    </parcour>
                <parcour numero="0" libelle="Automatisme et informatique industrielle" code="AII">
                        <annee ordre="1">
                                <competence niveau="1" id="b078d2f7f61c73d3d3c47a1b01b0ba4d"/>
                                <competence niveau="1" id="8d6ba092510bfa04de81c140fc69816d"/>
                            </annee>
                        <annee ordre="2">
                                <competence niveau="1" id="a1ca5c95c20b946c22eb8a1ce1047359"/>
                                <competence niveau="2" id="b078d2f7f61c73d3d3c47a1b01b0ba4d"/>
                                <competence niveau="2" id="8d6ba092510bfa04de81c140fc69816d"/>
                                <competence niveau="1" id="022ed4cb9c2b145cc22bc9ac0b41a245"/>
                            </annee>
                        <annee ordre="3">
                                <competence niveau="2" id="a1ca5c95c20b946c22eb8a1ce1047359"/>
                                <competence niveau="3" id="b078d2f7f61c73d3d3c47a1b01b0ba4d"/>
                                <competence niveau="3" id="8d6ba092510bfa04de81c140fc69816d"/>
                                <competence niveau="2" id="022ed4cb9c2b145cc22bc9ac0b41a245"/>
                            </annee>
                    </parcour>
            </parcours>
</referentiel_competence>
